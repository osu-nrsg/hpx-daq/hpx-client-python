# Changelog

All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Previous changelogs

- [HPx FrameMaker Changelog](https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker/-/blob/master/CHANGELOG.md)
- [HPx Messages Changelog](https://gitlab.com/osu-nrsg/hpx-daq/hpx-messages-python/-/blob/master/CHANGELOG.md)
- [HPx Server Monitor Changelog](https://gitlab.com/osu-nrsg/hpx-daq/hpx-server-monitor-python/-/blob/master/CHANGELOG.md)

## [Unreleased]

## [2.3.2] - 2024-05-30

- Use `numpy.typing.NDArray` to set static types on numpy arrays
- Make some small changes to appease mypy

## [2.3.1] - 2024-05-27

- Update docstrings
- Amend type annotations on `Frame` properties
- Minor cleanup

## [2.3.0] - 2024-05-23

- Migrate to Python 3.12
  - Use vertical bar syntax in lieu of Union and Optional.
  - Use new generic syntax where sensible
  - Use ExceptionGroups in new ExceptionQueue class.
- Switch from black and isort to ruff.
- Update to latests numpy and pyzmq
- New ExceptionQueue class for capturing thread/process exceptions and raising them in a
  parent thread/process.
- Improve commenting, docstrings, and type annotations
- Refactor FMConfig range bounds checking
- Refactor ServerMonitor start/reset functionality
- Reorder some methods

## [2.2.2] - 2024-04-10

- Improve error messages for bad azimuth configurations
- minor - import class from owning module
- minor formatting cleanup
- Use ray.proc_time for frame.time if not calc_true_timestamps. Fixes #1.
- lightweight refactors:
  - 90-char max width
  - use absolute imports

## [2.2.1]

- Add py.typed marker to package for mypy

## [2.2.0]

The main purpose of this release is to support continuous recording in hpx-radar-recorder. To implement
a frame peek it was necessary to drop multiprocessing support. This is overall fine since frame making is
IO-bound.

- MAJOR: FrameMaker - Removed multiprocessing option (frame making is mostly IO bound). Threading is
  sufficient.
- FrameMaker = Add an option to "peek" at the next frame by using `pop=false` in `FrameMaker.next_frame()`.
- FMConfig - minor refactor to use a contextmanager for using a pre-existing ServerMonitor
- Updated dependencies
- Clean-up of unneeded `#noqa`
- minor: Changed some comments, changes from new `black` version

## [2.1.0]

## General

- Use mypy to find and fix typing issues (either bad type annotation or actual errors)
- Various type annotation fixes
- Use py3.9 annotations (`dict`, `list`, `tuple`)
- Small Sourcery/Sonarlint suggested refactorings

## ConfigMsg

- Add `ord()` converter for `analog_channel`

### ServerMonitor

- Change thread to be an attribute rather than subclassing Thread.
- Start thread as daemon thread to ensure it stops at exit.
- Raise thread exceptions at exit

### FMConfig

- Accept a ServerMonitor as an argument to not need to wait for status again

### FrameMaker

- Merge FrameMaker and FrameProducer classes into a single FrameMaker with worker thread/process as an
  attribute rather than subclassing Thread or Process.
- Start worker as daemon thread/process to ensure it stops at exit.
- Raise worker exceptions at exit.

## [2.0.0]

- Merge [HPx FrameMaker] v0.4.1, [HPx Messages] v1.1.0, and [HPx Server Monitor] v1.0.0.

[HPx FrameMaker]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-framemaker
[HPx Server Monitor]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-server-monitor-python
[HPx Messages]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-messages-python

[Unreleased]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/compare/v2.3.2...master
[2.3.2]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/compare/v2.3.1...v2.3.2
[2.3.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/compare/v2.3.0...v2.3.1
[2.3.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/compare/v2.2.2...v2.3.0
[2.2.2]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/compare/v2.2.1...v2.2.2
[2.2.1]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/compare/v2.2.0...v2.2.1
[2.2.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/compare/v2.1.0...v2.2.0
[2.1.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/compare/v2.0.0...v2.1.0
[2.0.0]: https://gitlab.com/osu-nrsg/hpx-daq/hpx-client-python/-/tags/v2.0.0
