import time
from pprint import pprint

from hpx_client import ServerMonitor

if __name__ == "__main__":
    with ServerMonitor("ipc:///run/hpx-radar-server/hpx-pub.sock", 2.0) as servmon:
        servmon.wait_status_config()
        print(f"{'state':10s} | {'PRF':6s} | {'nrays'} | message")
        print("-" * 37)
        while True:
            stat = servmon.status
            print(
                f"{stat.state.name:10s} | {stat.avg_prf:6.1f} | {stat.meas_rays:5d} | {stat.message}"
            )
            time.sleep(2)
