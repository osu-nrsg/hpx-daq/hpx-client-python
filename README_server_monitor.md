# Server Monitor

The Server Monitor provides a Python interface to an
[HPx Radar Server](https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server) by subscribing to the `CONFIG` and
`STATUS` messages at the socket wither the server is publishing.

## Usage

ServerMonitor inherits from threading.Thread so it may be started with the threading `start()` method, and
calling `stop()` causes the server monitor to complete the thread execution (`stop()` does not join the
thread).

A context manager interface is also available to automatically start and stop the thread. Also when the
context manager exits the thread is joined.

```python
import time

from hpx_client.server_monitor import ServerMonitor

endpoint = "ipc://WIMR.sock"  # POSIX socket at ./WIMR.sock
status_timeout_s = 5  # Error if we ask for status/config and 5s without a new message
with ServerMonitor(endpoint, status_timeout_s) as sm:
    sm.wait_status_config()  # wait for the first status and config messages to arrive
    # From here on status and config are always updated to be whatever is in the latest messages
    for _ in range(10):  # print server state and config version 10x
        print(sm.status.state)  # See StatusMessage for fields
        print(sm.config.version)  # See ConfigMessage for fields
        time.sleep(1)
# ServerMonitor has stopped after `with` block
```

See docstrings for more details.

## `DummyServerMonitor`

`DummyServerMonitor` is a subclass of `ServerMonitor` that may be used for testing.

Class variables `DummyServerMonitor.dummy_status` and `DummyServerMonitor.dummy_config` must be set before
using `DummyServerMonitor`, but otherwise `DummyServerMonitor` may be used as a drop-in replacement for
`ServerMonitor`.
