# HPx Client (Python) README

This package includes a set of subpackages for interacting with an
[HPx Radar Server](https://gitlab.com/osu-nrsg/hpx-daq/hpx-radar-server).

## Messages

`hpx_client.messages` provides dataclasses for unpacking the ZeroMQ/MessagePack messages received from the
server. See [README_messages.md](README_messages.md).

## Server Monitor

`hpx_client.server_monitor` provides a ServerMonitor class for monitoring the status and configuration of
the server. See [README_server_monitor.md](README_server_monitor.md).

## FrameMaker

`hpx_client.framemaker` provides the FMConfig and FrameMaker classes for grouping received rays into
_frames_ for frame-by-frame processing. See [README_framemaker.md](README_framemaker.md).

## Exposed Classes and Functions

### Messages

- `ConfigMsg`
- `ConfigMsgError`
- `ServerState`
- `RayMsg`
- `StatusMsg`
- `TpgMode`

### Server Monitor

- `DummyServerMonitor`
- `HPxServerMonitorError`
- `ServerConfigChanged`
- `ServerMonitor`
- `ServerNotRunning`
- `ServerStatusTimeout`

### FrameMaker

- `FMConfig`
- `FMConfigError`
- `Frame`
- `FrameError`
- `FrameMaker`
- `FrameMakerError`
