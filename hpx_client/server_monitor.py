"""server_monitor.py - This module contains the ServerMonitor class."""

import atexit
import dataclasses
import logging
import queue
import threading
import time
from typing import Literal, overload

import zmq
import zmq.error

from hpx_client._exception_queue import ExceptionQueue

from .messages import ConfigMsg, ServerState, StatusMsg

logger = logging.getLogger(__name__)

__all__ = [
    "ServerMonitor",
    "DummyServerMonitor",
    "ServerMonitorError",
    "ServerStatusTimeout",
    "ServerConfigChanged",
    "ServerNotRunning",
]


class ServerMonitorError(RuntimeError):
    pass


class ServerStatusTimeout(ServerMonitorError):
    pass


class ServerConfigChanged(ServerMonitorError):
    pass


class ServerNotRunning(ServerMonitorError):
    pass


class ServerMonitor:
    """Monitor an HPx Radar Server config and status.

    See ServerMonitor.__init__() for input parameters.

    Launch the monitor with start(), stop it with stop().

    Attributes
    ----------
    endpoint : str
        The 0MQ endpoint from which STATUS and CONFIG messages are being received.
    status_timeout_s : float
        Configured number of seconds before a status timeout occurs
    config : ConfigMsg
        The most recently received CONFIG message.
    status : StatusMsg
        The most recently received STATUS message.
    config_changed : bool
        True if the config has changed since the ServerMonitor started.  Does not
        self-reset.
    status_timeout : bool
        True if no status message has been received in more than status_timeout_s. Resets
        when status is received.
    status_rcvd : bool
        A status message has been received at least once.
    config_rcvd : bool
        A config message has been received at least once.
    """

    # types
    endpoint: str  # zmq endpoint
    status_timeout_s: float
    _config: ConfigMsg | None = None
    _status: StatusMsg | None = None
    config_changed: bool
    status_timeout: bool
    status_rcvd: bool
    config_rcvd: bool

    _thread: threading.Thread
    _estop: threading.Event

    def __init__(self, endpoint: str, server_timeout_s: float, delay_start=False):
        """Create a ServerMonitor instance.

        Parameters
        ----------
        endpoint : str
            0MQ endpoint for the STATUS and CONFIG subscribers. E.g. ipc://<socket_path>
            or tcp://<ip_or_domain>:<port>
        server_timeout_s : float
            Seconds to go without a status message before raising a timeout error.
        delay_start
            If `False` (default), a thread is immediately started to begin monitoring the
            server for status and config messages. Otherwise, monitoring may be initiated
            with the `start()` method or by using the ServerMonitor instance in a context
            manager (`with` block). In addition, if `delay_start` is `False`, the
            `shutdown()` method is registered to stop the thread when Python exits.
        """
        self.endpoint = endpoint
        self.status_timeout_s = server_timeout_s
        self._delay_start = delay_start  # save for future use in `reset()`
        self.config_changed = False
        self.status_timeout = False
        self.status_rcvd = False
        self.config_rcvd = False

        # Set up a new thread
        self._thread = threading.Thread(target=self.run, daemon=True)
        self._estop = threading.Event()
        self._exc_q = ExceptionQueue(queue.Queue, 1)

        # Start the thread and register shutdown, unless told to delay.
        if not self._delay_start:
            atexit.register(self.shutdown)
            self.start()

    def reset(
        self,
        endpoint: str | None = None,
        server_timeout_s: float | None = None,
        skip_if_unchanged: bool = False,
    ):
        """Restart the ServerMonitor, optionally replacing initialization parameters. If
        the thread was originally started on initialization, it will be restarted
        immediately. See `__init__()` for more details on parameters.

        Parameters
        ----------
        endpoint
            New endpoint to use for 0MQ connection. If not provided, the original endpoint
            will be used.
        server_timeout_s
            New server timeout to use. If not provided, the original timeout will be used.
        skip_if_unchanged
            Don't restart the monitoring thread if it's already running and neither the
            endpoint nor the timeout has changed (default `False`).
        """
        if (
            skip_if_unchanged
            and self._thread.is_alive()
            and self.endpoint == endpoint
            and self.status_timeout_s == server_timeout_s
        ):
            return

        if self._thread.is_alive():
            self.shutdown()
        ServerMonitor.__init__(
            self,
            endpoint or self.endpoint,
            server_timeout_s or self.status_timeout_s,
            delay_start=self._delay_start,
        )

    def start(self):
        if not self._thread.is_alive():
            self._thread.start()

    def stop(self):
        """Stop the thread."""
        self._estop.set()

    def shutdown(self):
        self.stop()
        self.join()

    def join(self, timeout=None):
        """Join thread and get and raise thread exceptions

        Parameters
        ----------
        timeout
            timeout to pass to Thread.join()
        Raises
        ------
        BaseException
            Any exceptions thrown in the thread
        """
        self._thread.join(timeout)
        self._raise_thread_exceptions()

    def _raise_thread_exceptions(self):
        """Raise any exceptions caught in the monitor thread"""
        self._exc_q.raise_all()

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.shutdown()

    @property
    def running(self) -> bool:
        return self._thread.is_alive()  # True if still in `run()`.

    def run(self):
        """The main thread function. Wraps `_monitor()` and passes exceptions in _exc_q"""
        # Outer try, except gets all exceptions for pickling
        try:
            # Inner try, except wraps monitor errors in various ways
            try:
                self._monitor()
            except zmq.error.ZMQError as zerr:
                if "Protocol not supported" in zerr.strerror:
                    raise ServerMonitorError(
                        f"Invalid protocol in endpoint: '{self.endpoint}'"
                    ) from zerr
                if "Invalid argument" in zerr.strerror:
                    raise ServerMonitorError(
                        f"Invalid endpoint: '{self.endpoint}'"
                    ) from zerr
                raise ServerMonitorError from zerr
            except Exception as exc:
                raise ServerMonitorError from exc
        except Exception as exc:
            self._exc_q.put(exc)

    def _monitor(self):
        """
        Open subscriber sockets for CONFIG and STATUS messages and continue to receive new
        messages until told to stop via `stop()`.
        """
        # Open and configure 0MQ sockets
        context = zmq.Context.instance()
        config_sub = context.socket(zmq.SUB)
        status_sub = context.socket(zmq.SUB)
        config_sub.subscribe("CONFIG")
        status_sub.subscribe("STATUS")
        t_last_status = time.perf_counter()
        with config_sub.connect(self.endpoint), status_sub.connect(self.endpoint):
            # Run the main loop. Check for status and config messages, check the contents
            # of those messages, and save them.
            while not self._estop.is_set():
                if status := StatusMsg.fromsocket(status_sub):
                    t_last_status = time.perf_counter()
                    self._status = status
                    self.status_rcvd = True
                if config := ConfigMsg.fromsocket(config_sub):
                    if self._config:
                        if config != self._config:
                            self.config_changed = True
                    else:
                        self._config = config
                    self.config_rcvd = True
                # Set the status_timeout if too long has passed without a status message,
                # but don't latch.
                self.status_timeout = (
                    time.perf_counter() - t_last_status
                ) > self.status_timeout_s
                time.sleep(0.001)  # Don't need to check more frequently.

    def wait_status_config(self):
        """Block until status and config are set.

        Raises
        ------
        ServerStatusTimeout
            If no status received within status_timeout_s.
        """
        while not self._config or not self._status:
            if self.status_timeout:
                raise ServerStatusTimeout(
                    f"No status received within timeout limit: {self.status_timeout_s} s"
                )
            self._raise_thread_exceptions()  # Raise any thread exceptions (bad things)

    @property
    def status(self) -> StatusMsg:
        if self._status is None:
            raise RuntimeError(
                "Status has not yet been received. Run wait_status_config() first."
            )
        return self._status

    @property
    def config(self) -> ConfigMsg:
        if self._config is None:
            raise RuntimeError(
                "Config has not yet been received. Run wait_status_config() first."
            )
        return self._config

    def check_and_raise(
        self,
        check_timeout=True,
        check_notrunning=True,
        check_config_changed=True,
        raise_err=True,
    ) -> tuple[bool, bool, bool]:
        """Convenience method for raising common status and config errors.

        Parameters
        ----------
        check_timeout
            Enable checking for status timeout
        check_notrunning
            Enable checking that the server state is "running"
        check_config_changed
            Enable checking if the config has changed since the ServerMonitor instance was
            started.
        raise_err
            Enable raising a `RuntimeError` for each of the above checks. If False, a
            tuple of three bool values is returned, one for each check.

        Returns
        -------
        If `raise_err==True` (and there are no errors), `None` is returned. Otherwise, a
        tuple of three `bool`s is returned: `(timeout, notrunning, config_changed)`

        Raises
        ------
        ServerStatusTimeout
            No status message received within timeout period
        ServerNotRunning
            Server state is not "running"
        ServerConfigChanged
            There was a change in the server configuration (not including calculated or
            measured values)
        """
        self._raise_thread_exceptions()  # Raise any thread exceptions (bad things)
        timeout, notrunning, config_changed = False, False, False
        if check_timeout and self.status_timeout:
            if raise_err:
                raise ServerStatusTimeout(
                    f"No status message from the server in {self.status_timeout_s:g}"
                    " seconds."
                )
            timeout = True
        if check_notrunning and (self.status.state != ServerState.running):
            if raise_err:
                raise ServerNotRunning(
                    "Server is in a non-running state. Current server state:"
                    f" {self.status.state.name}"
                )
            notrunning = True
        if check_config_changed and self.config_changed:
            if raise_err:
                raise ServerConfigChanged("The server config changed.")
            config_changed = True
        return timeout, notrunning, config_changed


class DummyServerMonitor(ServerMonitor):
    """Dummy version of ServerMonitor

    Note: dummy_status and dummy_config class variables must be set before initialization.
    """

    dummy_status: StatusMsg
    dummy_config: ConfigMsg
    _status: StatusMsg
    _config: ConfigMsg

    def __init__(self, endpoint: str, server_timeout_s: float):
        """Initialize a DummyServerMonitor

        Parameters
        ----------
        endpoint
            Usually would be the ZeroMQ endpoint, but does nothing in DummyServerMonitor
        server_timeout_s
            Timeout for receiving status and config messages. Also divided by 4 for status
            & config period.

        Notes
        -----
        Class variables DummyServerMonitor.dummy_status and
        DummyServerMonitor.dummy_config must be set to dummy status and config messages,
        respectively, before initializing an instance.
        """
        self._dummy_update_period = server_timeout_s
        self._status = self.dummy_status
        self._config = self.dummy_config
        self.status_enabled = True
        self.config_enabled = True
        super().__init__("dummy", server_timeout_s)

    def _monitor(self):
        """Dummy override of ServerMonitor._monitor()

        Updates status and config every status_timeout_s / 4
        """
        update_period = self.status_timeout_s / 4
        tstart = time.time()
        tnext = tstart + update_period
        start_runtime = self.status.runtime_s
        orig_config = self._config
        # Assume status/config received if enabled.
        self.status_rcvd = self.status_enabled
        self.config_rcvd = self.config_enabled
        t_last_status = time.perf_counter()
        while not self._estop.is_set():
            print(f"{self.status_enabled:}")
            # Check for changed config and update the received status timee
            if self.config_enabled and (self._config != orig_config):
                self.config_changed = True
            if self.status_enabled:
                t_last_status = time.perf_counter()
                self.status.runtime_s = int(start_runtime + (time.time() - tstart))
            # Check for timeout of status messages
            self.status_timeout = (
                time.perf_counter() - t_last_status
            ) > self.status_timeout_s
            # Sleep till next loop if sleep time is not negative
            if (tsleep := tnext - time.time()) > 0:
                time.sleep(tsleep)
            tnext += update_period

    def update_status(self, **kwargs):
        """Modify specific fields of the status message"""
        self._status = dataclasses.replace(self._status, **kwargs)

    def update_config(self, **kwargs):
        """Modify specific fields of the config message"""
        self._config = dataclasses.replace(self._config, **kwargs)

    def toggle_status(self, enabled: bool):
        """Toggle dummy status message on/off"""
        self.status_enabled = enabled

    def toggle_config(self, enabled: bool):
        """Toggle dummy config message on/off"""
        self.config_enabled = enabled
