"""_version.py - Use single_version to get the package version either by parsing the
pyproject.toml file or getting it from importlib.metadata"""

from pathlib import Path

import single_version  # type: ignore

__version__ = single_version.get_version(__name__, Path(__file__).parent.parent)
