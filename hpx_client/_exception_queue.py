"""_exception_queue.py - Provides the ExceptionQueue class to make it easier to pass
exceptions between threads and processes."""

import queue
from typing import Callable, Protocol

from tblib import pickling_support  # type: ignore

# ensure pickle knows how to pickle all built-in exceptions
pickling_support.install()


class SimpleQueueType[T](Protocol):
    """Minimal Queue protocol for ExceptionQueue. multiprocessing.Queue and queue.Queue
    are both valid instances."""

    def __init__(self, maxsize: int): ...

    def put(self, obj: T, /, block: bool = True, timeout: float | None = None): ...

    def get(self, block: bool = True, timeout: float | None = None) -> T: ...

    def get_nowait(self) -> T: ...


class ExceptionQueue:
    """ExceptionQueue creates a queue.Queue, multiprocessing.Queue or similar to be used
    for transferring exceptions raised in one ore more threads or processes to another
    thread or process
    """

    def __init__(self, qfactory: Callable[[int], SimpleQueueType], maxsize: int = 0):
        """Initialize the ExceptionQueue.

        Parameters
        ----------
        qfactory
            Any callable that takes an integer maxsize argument and creates an object the
            follows the SimpleQueueType protocol. queue.Queue and multiprocessing.Queue
            are both valid.
        maxsize
            Maximum size of the queue created with qfactory. 0 usually means unbounded.
        """
        self._q: SimpleQueueType[BaseException] = qfactory(maxsize)

    def put(self, exc: BaseException):
        """Ensure the exception can be pickled and put it in the queue."""
        pickling_support.install(exc)
        self._q.put(exc)

    def raise_all(self):
        """Raise any and all exceptions in the queue. If there are more than one, they
        will be raised as a BaseExceptionGroup."""
        excs: list[BaseException] = []
        while True:
            try:
                excs.append(self._q.get_nowait())
            except queue.Empty:
                break
        if len(excs) == 1:
            raise excs[0]
        if len(excs) > 1:
            raise BaseExceptionGroup(
                "Multiple exceptions raised in threads or processes.",
                excs,
            )
