"""This module holds all exceptions for the project, and also adds pickling support. This
should be the last import in the main frame_maker module.
"""


class FrameMakerError(Exception):
    pass


class FMConfigError(FrameMakerError):
    pass


class FrameError(Exception):
    """General error class for frames."""

    pass


class FullFrame(FrameError):
    """Indicates that the frame is full."""

    def __init__(self):
        super().__init__("The frame has run out of allocated space for rays.")


class LockHeld(Exception):
    pass


class LockTimeout(LockHeld):
    def __init__(self, seconds: float | int, lock_typename: str):
        super().__init__(
            f"The lock is held by another {lock_typename} and could not be acquired in"
            f" {seconds:g} seconds"
        )
