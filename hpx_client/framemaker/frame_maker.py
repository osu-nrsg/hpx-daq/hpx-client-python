"""frame_maker.py - FrameMaker class"""

from __future__ import annotations

import atexit
import queue
import threading
import time
from collections.abc import Iterator
from contextlib import contextmanager, suppress
from typing import overload

import numpy as np
import zmq
from numpy.typing import NDArray

from hpx_client._exception_queue import ExceptionQueue
from hpx_client.framemaker.errors import FrameError, FrameMakerError
from hpx_client.framemaker.fmconfig import FMConfig, angle_in_range
from hpx_client.framemaker.frame import Frame
from hpx_client.framemaker.timed_lock import LockHeld
from hpx_client.messages import RayMsg


class FrameMaker:
    """A FrameMaker does the following:
    - Recieve rays from the HPx Radar Server
    - Build frames
    - Buffer frames to be consumed while subsequent frames are being built
    - Provide consumer access to the next frame in the buffer (FrameMaker.next_frame())
    """

    _worker: threading.Thread
    _estop: threading.Event
    _ready_q: queue.Queue[int]
    _exc_q: ExceptionQueue
    _framebuf: list[Frame]

    def __init__(self, config: FMConfig):
        """Set up a FrameMaker instance.

        Parameters
        ----------
        config
            FMConfig instance containing the pre-checked configuration parameters for the
            FrameMaker.

        Raises
        ------
        ValueError
            _framebuf_size is less than 2.
        """
        self.config: FMConfig = config
        self._frame_i: int = -1
        self._init_concurrency_primatives(config)
        self._init_framebuf(config)
        self._init_worker()
        atexit.register(self.stop)

    # ##
    # Init submethods
    # ##
    def _init_concurrency_primatives(self, config: FMConfig):
        """Set up event and queues for communicating with the worker"""
        self._estop = threading.Event()
        self._ready_q = queue.Queue(config.framebuf_size)
        self._exc_q = ExceptionQueue(queue.Queue, 1)

    def _init_framebuf(self, config: FMConfig):
        """Initialize the buffer of frames to transfer data between the producer (worker)
        and consumer"""
        # The frame buffer allows for processing one full frame while the next frame is
        # being built.
        # Here the frame buffer is preallocated.
        self._framebuf = [
            Frame(
                int(config.srays_last_sweep * config.frame_max_nrays_factor),
                config.nsamples,
                config.rg_slice,
            )
            for _ in range(config.framebuf_size)
        ]

    def _init_worker(self):
        self._worker = threading.Thread(target=self.run, daemon=True)

    # ##
    # Properties to access internal variables
    # ##
    @property
    def endpoint(self) -> str:
        return self.config.endpoint

    @property
    def nsamples(self) -> int:
        return self.config.nsamples

    @property
    def rg(self) -> NDArray[np.floating]:
        return self.config.rg

    # ##
    # pseudo-classmethod
    # ##
    def updated(self, config: FMConfig) -> FrameMaker:
        """Either keep the same framemaker if the config is unchanged or stop it and get a
        new one"""
        if self.is_alive():
            self.stop()
        return self.__class__(config)

    # ##
    # Worker state methods
    # ##
    def is_alive(self) -> bool:
        return self._worker.is_alive()

    def start(self):
        self._worker.start()

    def join(self):
        self._worker.join()

    def stop(self):
        if self.is_alive():
            self._estop.set()
            self.join()
        self.reraise_exceptions()

    # ##
    # Context manager methods
    # ##
    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    # ##
    # Worker methods to receive rays, create threads, and push them to the queue
    # ##
    def run(self):
        """Thread/Process target.

        Wraps the main function of the thread/process to handle any exceptions from that
        function.
        """
        try:
            self._create_frames()
        except Exception as exc:
            self._exc_q.put(exc)

    def _create_frames(self):
        """Receive rays from the 0mq endpoint and create frames."""
        ray = RayMsg.preallocate(self.config.server_nsamples)
        last_ray_azival = None

        # Get the worker buffer
        worker_buf = self._framebuf

        ray_sub: zmq.Socket = zmq.Context.instance().socket(zmq.SUB)
        ray_sub.setsockopt(zmq.RCVTIMEO, 1)  # Minimize loop period
        ray_sub.subscribe("RAY")  # Make HWM max size of framebuf
        ray_sub.set_hwm(len(worker_buf) * worker_buf[0].max_nrays)
        with ray_sub.connect(self.config.endpoint):
            while not self._estop.is_set():
                if not ray.update_fromsocket(ray_sub):
                    # No new ray. Try again.
                    continue
                if last_ray_azival is not None:
                    self._handle_ray(ray, last_ray_azival, worker_buf)
                last_ray_azival = ray.azimuth

    def _handle_ray(self, ray: RayMsg, last_ray_azival: np.uint16, framebuf: list[Frame]):
        """Check a received ray and add it to a frame if it is to be kept. Also push the
        frame onto the ready queue if it is full.

        Parameters
        ----------
        ray
            The ray to be handled
        last_ray_azival
            The azimuth value (ACP count) from the previously handled ray
        framebuf
            The buffer to which frames are written.

        self._frame_i is the index of the current frame being built
        """
        # Check the ray azimuth and determine if this is a new frame
        ray_azi_in_bounds, new_frame = self._check_ray_azi(ray, last_ray_azival)

        # Do nothing if:
        #   - The azimuth is outside the bounds we're keeping
        #   - The first frame boundary (start_azi) has not been reached.
        #   - The ray is filtered
        if (
            not ray_azi_in_bounds
            or (self._frame_i == -1 and not new_frame)
            or ray.filtered
        ):
            return

        # Is the current ray the start of a new frame?
        if new_frame:
            # Have we already filed a frame?
            if self._frame_i > -1:
                # If so, we can signal to the consumer that the full frame is ready to
                # process.
                qpush(self._ready_q, self._frame_i)
            # update the frame index
            self._frame_i = (self._frame_i + 1) % len(framebuf)
            # Reset the now-current frame
            try:
                framebuf[self._frame_i].reset()
            except LockHeld as lock_err:
                raise FrameMakerError(
                    "The FrameMaker cannot reset the next frame in the buffer as there is"
                    " currently a lock on that frame from a reader thread/process. Speed"
                    " up the reader or increase framebuf_size in the config."
                ) from lock_err

        # If we're only collecting up until srays_per_frame and the current frame has
        # reached that limit, then don't add any more rays to the frame (wait for the next
        # frame to start)
        if (
            self.config.srays_per_frame is not None
            and framebuf[self._frame_i].nrays >= self.config.srays_per_frame
        ):
            return

        # By reaching here all early return criteria have been overcome
        try:
            framebuf[self._frame_i].add_ray(ray)
        except LockHeld as lock_err:
            raise FrameMakerError(
                "The FrameMaker cannot add rays to a new frame as there is currently a"
                " lock on that frame from a reader thread/process. Speed up the reader or"
                " increase framebuf_size in the config."
            ) from lock_err
        except FrameError as fr_err:
            raise FrameMakerError(
                "The Frame ran out of room for new rays. Either increase the"
                " frame_max_nrays_factor or ensure that the radar is still rotating."
            ) from fr_err

    def _check_ray_azi(
        self, ray: RayMsg, last_ray_azival: np.uint16
    ) -> tuple[bool, bool]:
        """Check if a ray is in the azi bounds and if it starts a new rotation

        Parameters
        ----------
        ray
            The current RayMsg.
        last_ray_azival
            The ACP count from the last ray.

        Returns
        -------
        tuple
            - azi_in_bounds: The ray is within the desired azimuth bounds
            - new_frame: This ray is the first in a new frame
        """
        # Check that the current ray's azimuth is within the azimuth bounds
        ray_azid = float(ray.azimuth) * 360.0 / 2**16
        azi_in_bounds = (self.config.end_azi is None) or angle_in_range(
            ray_azid, self.config.start_azi, self.config.end_azi
        )

        # Check for new frame by comparing the current ray azi and the last ray azi
        # relative to start_azi
        azid0 = (ray_azid - self.config.start_azi) % 360.0
        last_azid = float(last_ray_azival) * 360.0 / 2**16
        last_azid0 = (last_azid - self.config.start_azi) % 360.0
        new_frame = azid0 < last_azid0

        return azi_in_bounds, new_frame

    # ##
    # Consumer methods to get frames from the frame queue
    # ##
    def reraise_exceptions(self):
        """Raise exceptions from the thread or process.

        This should be run for any actions that interact with frames.
        """
        self._exc_q.raise_all()

    @overload
    @contextmanager
    def next_frame(self, timeout: None = None, pop: bool = True) -> Iterator[Frame]: ...

    @overload
    @contextmanager
    def next_frame(
        self, timeout: float = ..., pop: bool = True
    ) -> Iterator[Frame | None]: ...

    @contextmanager
    def next_frame(
        self, timeout: float | None = None, pop=True
    ) -> Iterator[Frame | None]:
        """Context manager for getting and locking access to the data from a frame.

        This will raise any exceptions from the worker thread/process each time it is
        called.

        Parameters
        ----------
        timeout
            Max seconds to wait for a new frame. Default is None (infinite). If no frame
            is available before the timeout, None is returned.
        pop
            If false, the frame is not removed from the queue. This may be useful to
            "peek" at contents of the frame before consuming it.
        """
        self.reraise_exceptions()
        try:
            if pop:
                frame_i = self._ready_q.get(timeout=timeout)
            else:
                frame_i = qpeek(self._ready_q, timeout=timeout)
            with self._framebuf[frame_i].lock:
                yield self._framebuf[frame_i]
        except queue.Empty:
            yield None


# #############################
# ## Queue utility functions ##
# #############################


def qpeek[T](q: queue.Queue[T], block=True, timeout: float | None = None) -> T:
    """Peek at the next element of a Queue without removing it. Takes advantage of being
    able to index the underlying deque to peek at the first element.

    The blocking/timeout code is code is copied from Queue.get().

    Parameters
    ----------
    q
        The queue from which to get the next element
    block
        If the queue is empty, wait until an element arrives (or timeout is exceeded, if
        set).
    timeout
        If block is True, seconds to wait until raising an error.

    Raises
    ------
    queue.Empty
        queue is empty and block is False or timeout has been exceeded
    """
    with q.not_empty:
        if not block:
            if not q._qsize():
                raise queue.Empty
        elif timeout is None:
            while not q._qsize():
                q.not_empty.wait()
        elif timeout < 0:
            raise ValueError("'timeout' must be a non-negative number")
        else:
            endtime = time.monotonic() + timeout
            while not q._qsize():
                remaining = endtime - time.monotonic()
                if remaining <= 0.0:
                    raise queue.Empty
                q.not_empty.wait(remaining)
        return q.queue[0]  # actual peek here


def qpush[T](q: queue.Queue[T], item: T):
    """Continue to try to put an item on a queue, removing items from the back as
    necessary."""
    while True:
        try:
            q.put_nowait(item)
        except queue.Full:
            with suppress(queue.Empty):
                q.get_nowait()
        else:
            break


def qget[T](q: queue.Queue[T], default: T) -> T:
    """Try to get an item from a queue and return `default` if the queue is empty."""
    try:
        return q.get_nowait()
    except queue.Empty:
        return default
