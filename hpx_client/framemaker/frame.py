"""
Class for radar frames.
"""

import logging
import threading

import numpy as np
from numpy.typing import NDArray

from hpx_client.framemaker.errors import FullFrame
from hpx_client.framemaker.timed_lock import TimedLock
from hpx_client.messages import RayMsg

logger = logging.getLogger(__name__)


class Frame:
    """Abstraction of a [portion of] a single sweep of radar data.

    A frame is preallocated to a certain maximum size but with a virtual size of 0. As
    rays are added to the frame, the virtual size increases until the maximum size (at
    which point a `FullFrame` exception is raised if more rays are added). Each of the
    `data`, `azimuth`, and `time` properties returns the portion of the preallocated
    buffer for that frame attribute determined by the virtual size. When the frame is
    reset, the virtual size is set back to 0.

    The frame holds a lock object that is acquired for adding rays or resetting the frame.
    Users of the frame should acquire the lock while accessing the frame's data.
    """

    def __init__(
        self,
        max_nrays: int,
        nsamples: int,
        rg_slice: slice,
        true_timestamps: bool = True,
    ):
        """Set up a Frame instance and preallocate space for the frame data.

        Parameters
        ----------
        max_nrays
            Number of rays to preallocate into the frame. The frame cannot grow larger
            than this, so make it conservatively big.
        nsamples
            Number of samples in each ray. This cannot change for the lifetime of the
            Frame.
        rg_slice
            Slice object to apply to the range dimension of the ray data before storing in
            the fraame.
        """
        self._max_nrays: int = max_nrays
        self._nsamples: int = nsamples
        self._rg_slice: slice = rg_slice
        self._true_timestamps = true_timestamps

        # Set up the data storage arrays
        self._data: NDArray[np.uint16] = np.ndarray(
            shape=(max_nrays, nsamples), dtype=np.uint16
        )
        self._azi: NDArray[np.uint16] = np.ndarray(shape=(max_nrays,), dtype=np.uint16)
        self._time: NDArray[np.datetime64] = np.ndarray(
            shape=(max_nrays,), dtype="datetime64[ns]"
        )
        self._nrays = 0  # set _nrays_ with the _nrays setter
        self.lock = threading.Lock()

    def __str__(self):
        return (
            f"{__name__.split('.')[0]}.Frame with {self.nrays} rays,"
            f" {self._nsamples} samples, beginning at time {self.time[0]}"
        )

    @property
    def max_nrays(self) -> int:
        return self._max_nrays

    @property
    def nrays(self) -> int:
        return self._nrays

    @property
    def data(self) -> NDArray[np.uint16]:
        """The radar intensity data of the frame [np.uint16]"""
        return self._data[: self._nrays, :]

    @property
    def azival(self) -> NDArray[np.uint16]:
        """The ACP count values for each ray of the frame. [np.uint16]"""
        return self._azi[: self._nrays]

    @property
    def azi(self) -> NDArray[np.float32]:
        """Azimuth angles in degrees for each ray of the frame. [np.float32]"""
        return np.float32(360) * self.azival / 65536

    @property
    def time(self) -> NDArray[np.datetime64]:
        """The timestamps for each ray of the frame. [np.datetime64]"""
        return self._time[: self._nrays]

    @property
    def azi_smoothed(self) -> NDArray[np.float32]:
        """Azimuth angles in degrees with duplicates removed by smoothing"""
        azi_to_smooth = np.unwrap(self.azi * np.pi / 180.0)  # unwrapped radians
        smoothed_azi = azi_to_smooth.copy()
        # Get the index of the first of each pair of duplicates
        dup_azi_i = np.nonzero(np.diff(azi_to_smooth) == 0)[0]
        for i, k in zip(dup_azi_i[:-1], dup_azi_i[1:]):
            # Find the slope between pairs of duplicates and use that to recreate the
            # azimuths inbetween
            slope = (azi_to_smooth[k] - azi_to_smooth[i]) / (k - i)
            smoothed_azi[i:k] = azi_to_smooth[i] + np.arange(k - i) * slope
        return (smoothed_azi * 180.0 / np.pi) % 360.0  # wrapped degrees

    def reset(self):
        """Reset the ray to empty.

        Raises
        ------
        LockHeld
            The lock could not be acquired (e.g. a consumer is holding the lock).
        """
        with TimedLock(self.lock, 0):
            self._nrays = 0

    def add_ray(self, ray_msg: RayMsg):
        """Add the data to the frame, growing the frame if necessary.

        Parameters
        ----------
        ray_msg
            HPx ray message.

        Raises
        ------
        FullFrame
            The the frame is full and can take no more rays.
        LockHeld
            The lock could not be acquired (e.g. a consumer is holding the lock).
        """
        with TimedLock(self.lock, 0):
            if self._nrays >= self._data.shape[0]:
                raise FullFrame
            ray_i = self._nrays
            self._data[ray_i, :] = ray_msg.data[self._rg_slice]
            self._azi[ray_i] = ray_msg.azimuth
            self._time[ray_i] = (
                ray_msg.calc_time if self._true_timestamps else ray_msg.proc_time
            )
            self._nrays += 1
