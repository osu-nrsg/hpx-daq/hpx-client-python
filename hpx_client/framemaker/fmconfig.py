"""FMConfig class"""

from __future__ import annotations

import dataclasses
from collections.abc import Iterator
from contextlib import contextmanager
from dataclasses import InitVar, dataclass, field
from functools import WRAPPER_ASSIGNMENTS, update_wrapper
from typing import Any, overload

import numpy as np
from numpy.typing import NDArray

from hpx_client.framemaker.errors import FMConfigError
from hpx_client.messages import ConfigMsg
from hpx_client.server_monitor import ServerMonitor


@dataclass(frozen=True)
class FMConfig:
    """Configuration object for FrameMaker.

    After initialization, checks the config against the HPx server configuration;
    therefore the HPx radar server must be running.

    Attributes
    ----------
    endpoint
        0MQ endpoint for the HPx Radar Server
    start_azi
        Minimum azimuth value for each frame, in degrees. (The first ray on or after this
        azimuth is the first ray of the frame). Default is 0.
    end_azi
        Optional stop azimuth value for each frame, in degrees, if defined. If not defined
        and srays_per_frame is also not defined, then the full sweep will be included in
        each frame.
    srays_per_frame
        May be defined instead of end_azi to have a fixed number of (summed) rays in every
        frame.
    min_range
        Minimum range to include in frame. Defaults to the minimum range of the HPx
        server. May not be smaller than the minimum range of the HPx server.
    max_range
        Maximum range to include in the frame. Defaults to the full range of the HPx
        server. May not be larger than the range of the HPx server as determined by the
        number of samples from the server.
    num_samples
        May be defined instead of `max_range` as the number of samples to record from each
        ray in each frame.
    frame_max_nrays_factor
        Tuning parameter. On initialization, the HPx server is checked for the number of
        summed rays in the last sweep. That number is multiplied by this factor to
        determine the amount of space to allocate for rays in each frame.
    framebuf_size
        Tuning paramter. This is the number of frames allocated in the frame buffer. If a
        single frame processing operation may take a long time but the average frame
        processing time is still below the rate at which frames are produced, this value
        may be increased (at the cost of memory) to keep more frames in memory.
    """

    endpoint: str
    start_azi: float = 0.0
    end_azi: float | None = None
    srays_per_frame: int | None = None
    min_range: float | None = None
    max_range: float | None = None
    num_samples: int | None = None
    frame_max_nrays_factor: float = 2.0
    framebuf_size: int = 3
    servmon: InitVar[ServerMonitor | None] = None  # NOSONAR
    # The below fields are generated in __post_init__
    frameend_criterion: str = field(init=False)
    binsize: int = field(init=False)
    rg_slice: slice = field(init=False)
    rg: NDArray[np.floating] = field(init=False)
    nsamples: int = field(init=False)
    srays_last_sweep: int = field(init=False)
    server_nsamples: int = field(init=False)
    server_calc_true_timestamps: bool = field(init=False)

    def _setattr(self, name: str, value: Any):
        """Workaround frozen dataclass behavior (only for use in __post_init__)"""
        object.__setattr__(self, name, value)

    def __post_init__(self, servmon=None):
        """Modify and validate dataclass fields"""
        # Make sure azimuths are in [0, 360)
        self._setattr("start_azi", self.start_azi % 360)
        if self.end_azi:
            self._setattr("end_azi", self.end_azi % 360)
        # Only spec num_samples or max_range or neither
        if self.num_samples is not None and self.max_range is not None:
            raise FMConfigError(
                "Only one of num_samples and max_range may be in an FMConfig."
            )
        # Set the criterion to be used to determine when to start a new frame
        self._setattr(
            "frameend_criterion",
            "num_srays" if self.srays_per_frame is not None else "end_azi",
        )
        # Connect to the HPx server and get some parameters from the server
        server_config, srays_last_sweep = self._get_server_config(servmon)
        # Check that the FrameMaker azimuth bounds are acceptable
        if self.frameend_criterion == "num_srays":
            # Use the number of summed rays requested to estimate an end_azi
            self._check_azi_bounds_num_srays(srays_last_sweep, server_config)
        else:
            # use the specified FMConfig end_azi
            self._check_azi_bounds(
                self.start_azi,
                self.end_azi,
                server_config.start_azid,
                server_config.end_azid,
            )
        # Merge the FrameMaker and server range configuration to get the range bounds
        binsize, rg_slice, rg, nsamples = self._get_range_bounds(server_config)
        # set FMConfig range attrs
        self._setattr("binsize", binsize)
        self._setattr("rg_slice", rg_slice)
        self._setattr("rg", rg)
        self._setattr("nsamples", nsamples)
        # Save a couple server config values
        self._setattr("srays_last_sweep", srays_last_sweep)
        self._setattr("server_nsamples", server_config.num_samples)
        self._setattr("server_calc_true_timestamps", server_config.calc_true_timestamps)
        # Check frame maker tuning params
        if self.frame_max_nrays_factor < 1:
            raise FMConfigError(
                "It is not advisable to allocate less than the measured number of rays"
                " per sweep in each Frame."
            )
        if self.framebuf_size < 2:
            raise FMConfigError("framebuf_size may not be less than 2.")

    def replace(self, /, **kwargs):
        """Return a copy of the dataclass with the specified fields replaced"""
        try:
            return dataclasses.replace(self, **kwargs)
        except TypeError as err:
            if "unexpected keyword" in err.args[0]:
                raise TypeError(
                    "One of the specified fields is not part of this dataclass."
                ) from err
            else:
                raise

    @contextmanager
    def _maybe_server_monitor(
        self, servmon: ServerMonitor | None
    ) -> Iterator[ServerMonitor]:
        """Context manager to wrap either an existing ServerMonitor or temporarily create
        a new one"""
        if servmon:
            yield servmon
        else:
            with ServerMonitor(self.endpoint, 5) as tmpsrvmon:
                yield tmpsrvmon

    def _get_server_config(
        self, servmon: ServerMonitor | None = None
    ) -> tuple[ConfigMsg, int]:
        """Get status and config information from the HPx server necessary for setting up
        the FrameMaker.

        Parameters
        ----------
        servmon
            Optional pre-existing server monitor to use for getting the server config (to
            save time waiting for a config message from a new ServerMonitor instance).

        Returns
        -------
        tuple
            server_config - Configuration of the hpx-radar-server srays_last_sweep -
            Number of summed rays in the most recent sweep of the radar.

        Raises
        ------
        hpx_server_monitor.ServerStatusTimeout
            If there's a timeout getting status and config from the server
        """
        # Get HPx config parameters
        with self._maybe_server_monitor(servmon) as servmon:
            servmon.wait_status_config()
            server_config = servmon.config
            srays_last_sweep = servmon.status.meas_rays // servmon.config.summing_factor
        return server_config, srays_last_sweep

    @staticmethod
    def _check_azi_bounds(
        fm_start_azi: float,
        fm_end_azi: float | None,
        server_start_azid: float,
        server_end_azid: float,
    ):
        """Ensure that the FrameMaker azi bounds are within the hpx server azimuth bounds.

        Parameters
        ----------
        fm_start_azi
            The FrameMaker's configured start azimuth in degrees.
        fm_end_azi
            The FrameMaker's configured end azimuth in degrees. If `None`, full 360 degree
            coverage is expected.
        server_start_azid
            The HPx server's start azimuth in degrees
        server_end_azid
            The HPx server's end azimuth in degrees

        Raises
        -------
        FMConfigError
            There is a conflict between the FrameMaker's configured azimuth bounds and the
            HPx server azimuth boundaries.
        """
        if server_end_azid == server_start_azid:
            # NOTE: EARLY EXIT.
            return
        # Easy check for conflicting 360 covg.
        if fm_end_azi is None or (fm_start_azi == fm_end_azi):
            errmsg_end_azi = f"{fm_end_azi:.2f}" if fm_end_azi is not None else "--None--"
            raise FMConfigError(
                "The HPx server does not provide full 360-degree coverage but the"
                " FrameMaker is configured for 360-degree coverage."
                " Server azimuth bounds (deg):"
                f" ({server_start_azid:.2f}, {server_end_azid:.2f}),"
                " FrameMaker azimuth bounds (deg):"
                f" ({fm_start_azi:.2f}, {errmsg_end_azi})"
            )
        # NOTE: Below here, we know that the HPx azi config is *not 360* and fm_end_azi
        # *is set*.
        # ## Check that FrameMaker boundaries are within the HPx bounds.
        if not angle_in_range(fm_start_azi, server_start_azid, server_end_azid):
            raise FMConfigError(
                f"The requested start azimuth {fm_start_azi:.1f} is not in the HPx"
                " server's azimuth bounds:"
                f" ({server_start_azid:.2f}, {server_end_azid:.2f})"
            )

        if not angle_in_range(fm_end_azi, server_start_azid, server_end_azid):
            raise FMConfigError(
                f"The requested or calculated end azimuth {fm_end_azi:.1f} is not in the"
                " HPx server's azimuth bounds:"
                f" ({server_start_azid:.2f}, {server_end_azid:.2f})"
            )
        # ## Check if framemaker bounds cross server_start_azid

        if fm_end_azi is not None:
            fm_start_relative = (fm_start_azi - server_start_azid) % 360
            fm_end_relative = (fm_end_azi - server_start_azid) % 360
            if fm_end_relative < fm_start_relative:
                raise FMConfigError(
                    f"Frame maker azimuth bounds ({fm_start_azi:.1f}, {fm_end_azi:.1f})"
                    " eclipse server azimuth bounds"
                    f" ({server_start_azid:.1f}, {server_end_azid:.1f})"
                )

    def _check_azi_bounds_num_srays(
        self, srays_last_sweep: int, server_config: ConfigMsg
    ):
        """Check if the desired number of summed rays per frame is possible.

        Parameters
        ----------
        srays_last_sweep
            The number of summed rays in the most recent radar sweep.
        server_config
            hpx-radar-server config

        Raises
        ------
        FMConfigError
            The requested number of summed rays per sweep is not possible with the current
            HPx server config.
        """

        degrees_per_sray = 360.0 / srays_last_sweep
        degrees_per_sray *= 1.05  # prf can dip 5% or antenna can speed up 5%

        assert isinstance(self.srays_per_frame, int)

        if (degrees_per_sray * self.srays_per_frame) > 360:
            raise FMConfigError(
                "The number of requested summed rays per frame may cover more than 360"
                " degrees, considering current PRF, rotation rate, and 5% margin of"
                " error."
            )

        est_end_azi = np.mod(
            self.start_azi + (self.srays_per_frame * degrees_per_sray), 360
        )

        # Check this estimated azimuth boundary against the hpx server bounds
        self._check_azi_bounds(
            self.start_azi, est_end_azi, server_config.start_azid, server_config.end_azid
        )

    def _get_range_bounds(
        self, server_config: ConfigMsg
    ) -> tuple[float, slice, NDArray[np.floating], int]:
        """Determine the actual start & end range, range bin size, and number of samples,
        based on the input range bounds and the server config.

        Returns
        -------
        tuple
            binsize, range_slice, rg, nsamples

            - `binsize` is the size of each range bin, in meters
            - `range_slice` is a `slice` object for getting the selected range bins of the
              each ray from the
               server
            - `rg` is the range vector, in meters (the size of `range_slice`)
            - `nsamples` is the size of the range slice

        Raises
        ------
        FMConfigError
            There is a mismatch between the input range bounds and the server range
            bounds.
        """
        # get some aliases for brevity and clarity
        server_start_range = server_config.calc_start_range_metres
        server_end_range = server_config.calc_end_range_metres
        server_nsamples = server_config.num_samples
        server_range = np.linspace(server_start_range, server_end_range, server_nsamples)
        fm_min_range = self.min_range
        fm_max_range = self.max_range
        fm_nsamples = self.num_samples
        binsize = (server_end_range - server_start_range) / server_nsamples

        # Check or calculate the starting range index
        if fm_min_range is None:
            # No FrameMaker limit specified; use the first bin.
            start_range_i = 0
        elif fm_min_range >= server_start_range:
            # Get the range bin nearest to the requested start range.
            start_range_i = int(np.argmin(abs(server_range - fm_min_range)))
        else:
            raise FMConfigError(
                f"Specified FrameMaker min_range: {fm_min_range:.1f} is less than the"
                f" start range provided by the server: {server_start_range:.1f}",
            )

        # Check or calculate the ending range index. First try to use the FrameMaker
        # max_range, if it's defined. If not, try to use the FrameMaker num_samples, if
        # that's defined. Otherwise just take the last index of server_range.
        if fm_max_range is not None:
            if fm_max_range > server_end_range:
                raise FMConfigError(
                    f"Specified FrameMaker max_range: {fm_max_range:.1f} is greater than"
                    f" the end range provided by the server: {server_end_range:.1f}",
                )
            # Get the range bin nearest to the requested end range
            end_range_i = int(np.argmin(abs(server_range - fm_max_range)))
        elif fm_nsamples is not None:
            # Check if the desired number of samples can be gotten
            if start_range_i + fm_nsamples > server_nsamples:
                raise FMConfigError(
                    f"Requested number of samples: {fm_nsamples:.1f}"
                    " results in an end range greater than that provided by the server:"
                    f" {server_end_range:.1f}"
                )
            end_range_i = start_range_i + fm_nsamples - 1
        else:
            # Neither a maximum range or number of samples is defined, so just use the
            # last index of server_range.
            end_range_i = len(server_range) - 1

        range_slice = slice(start_range_i, end_range_i + 1)
        rg = server_range[range_slice]
        nsamples = int((end_range_i - start_range_i) + 1)
        return binsize, range_slice, rg, nsamples


# Make the replace() function look like __init__() (except keep its docstring)
update_wrapper(
    FMConfig.replace,
    FMConfig.__init__,
    assigned=[v for v in WRAPPER_ASSIGNMENTS if v != "__doc__"],
)


@overload
def angle_in_range(
    angle: float | int,
    bound_start: float | int,
    bound_end: float | int,
    units_per_rot: float | int = 360,
) -> bool: ...


@overload
def angle_in_range(
    angle: NDArray[np.number],
    bound_start: float | int,
    bound_end: float | int,
    units_per_rot: float | int = 360,
) -> NDArray[np.bool_]: ...


def angle_in_range(
    angle: float | int | NDArray[np.number],
    bound_start: float | int,
    bound_end: float | int,
    units_per_rot: float | int = 360,
) -> bool | NDArray[np.bool_]:
    """Determine if an angle is with in the bounds [bound_start, bound_end).

    Parameters
    ----------
    angle
        Angle(s) to test
    bound_start
        Start boundary
    bound_end
        End boundary
    units_per_rot : optional
        angle units per rotation. Default is 360 (degrees). Could be 2*pi for radians or
        something else.

    Returns
    -------
    bool or ndarray
        True if in range, False if not. ndarray of bools if angle is ndarray.
    """
    angle = np.mod(angle, units_per_rot)
    while bound_end <= bound_start:
        bound_end += units_per_rot
    while np.any(angle < bound_start):
        if isinstance(angle, np.ndarray):
            angle[angle < bound_start] = angle[angle < bound_start] + units_per_rot
        else:
            angle += units_per_rot
    # `return bound_start <= angle < bound_end` doesn't work with multi-dim np arrays
    return np.logical_and(bound_start <= angle, angle < bound_end)
