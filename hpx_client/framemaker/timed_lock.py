import threading

from hpx_client.framemaker.errors import LockHeld, LockTimeout


class TimedLock:
    """Simple wrapper around a lock to enforce a timeout and have a context manager"""

    def __init__(self, lock: threading.Lock, timeout: float):
        """

        Parameters
        ----------
        lock : multiprocessing.Lock or threading.Lock
            Lock instance
        timeout
            timeout to enforce (in seconds)
        """
        self._lock = lock
        self._timeout = timeout

    def __enter__(self):
        """Attempt to acquire the lock.

        Raise LockTimeout if acquisition times out or LockHeld if there was no timeout and
        the lock is held by another worker."""
        locked = self._lock.acquire(timeout=self._timeout)
        if not locked:
            lock_typename = "thread"
            if self._timeout > 0:
                raise LockTimeout(self._timeout, lock_typename)
            else:
                raise LockHeld(f"The lock is held by another {lock_typename}.")

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Release the lock"""
        self._lock.release()
