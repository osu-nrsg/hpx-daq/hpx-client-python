import multiprocessing
import queue
import threading

import pytest

from hpx_client._exception_queue import ExceptionQueue


def work(q: ExceptionQueue, worker_i: int, do_err: bool):
    if not do_err:
        return
    try:
        raise RuntimeError(f"Error in worker {worker_i}")
    except Exception as exc:
        q.put(exc)


def test_ExceptionQueue_two_threads():
    q = ExceptionQueue(queue.Queue)
    worker_1 = threading.Thread(target=work, args=(q, 1, True))
    worker_1.start()
    worker_1.join()
    with pytest.raises(RuntimeError, match="worker 1"):
        q.raise_all()


def test_ExceptionQueue_multithreading():
    q = ExceptionQueue(queue.Queue)
    worker_1 = threading.Thread(target=work, args=(q, 1, True))
    worker_2 = threading.Thread(target=work, args=(q, 2, False))
    worker_3 = threading.Thread(target=work, args=(q, 3, True))
    worker_1.start()
    worker_2.start()
    worker_3.start()
    worker_1.join()
    worker_2.join()
    worker_3.join()
    with pytest.raises(ExceptionGroup) as excinfo:
        q.raise_all()
    assert excinfo.group_contains(RuntimeError, match="worker 1")
    assert not excinfo.group_contains(RuntimeError, match="worker 2")
    assert excinfo.group_contains(RuntimeError, match="worker 3")


def test_ExceptionQueue_multiproc():
    q = ExceptionQueue(multiprocessing.Queue)
    worker_1 = multiprocessing.Process(target=work, args=(q, 1, True))
    worker_2 = multiprocessing.Process(target=work, args=(q, 2, False))
    worker_3 = multiprocessing.Process(target=work, args=(q, 3, True))
    worker_1.start()
    worker_2.start()
    worker_3.start()
    worker_1.join()
    worker_2.join()
    worker_3.join()
    with pytest.raises(ExceptionGroup) as excinfo:
        q.raise_all()
    assert excinfo.group_contains(RuntimeError, match="worker 1")
    assert not excinfo.group_contains(RuntimeError, match="worker 2")
    assert excinfo.group_contains(RuntimeError, match="worker 3")


if __name__ == "__main__":
    test_ExceptionQueue_multithreading()
    test_ExceptionQueue_multiproc()
