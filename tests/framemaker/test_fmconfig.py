"""test_fmconfig.py - Tests for hpx_framemaker.FMConfig"""

import dataclasses
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any, Type

import pytest

from hpx_client import (
    ConfigMsg,
    DummyServerMonitor,
    FMConfig,
    FMConfigError,
    ServerState,
    StatusMsg,
    TpgMode,
    framemaker,
)

#: StatusMsg for the DummyServerMonitor
dummy_status = StatusMsg(
    state=ServerState.running,
    avg_prf=1210.6830673866195,
    meas_azimuths=359,
    meas_rays=1703,  # rays/rot
    runtime_s=3,
    message="",
)

#: ConfigMsg for the DummyServerMonitor
dummy_config = ConfigMsg(
    board_idx=0,
    analog_channel="A",
    num_samples=4096,
    start_range_metres=478.0,
    range_correction_metres=-1400.0,
    voltage_offset=2.75,
    gain=-1.0,
    tpg_enabled=False,
    tpg_trigger_frequency=2000.0,
    tpg_acp_frequency=2560.0,
    tpg_arp_frequency=2048,
    enable_azimuth_interpolation=True,
    status_and_config_period_ms=500,
    spokes_per_interrupt=-1,
    interrupts_per_second=-1,
    calc_true_timestamps=True,
    summing_factor=6,
    start_azival=0,
    end_azival=0,
    send_filtered=True,
    card_name="HPx-200",
    fpga_version=17,
    calc_start_range_metres=476.7007870800003,
    calc_end_range_metres=12756.199866760002,
    meas_spokes_per_interrupt=41,
    version="0.6.0",
    tpg_mode=TpgMode.analog_ramp,
    bit_depth=12,
)

# Even if these will be updated, they need to be set first before they can be modified.
DummyServerMonitor.dummy_config = dummy_config
DummyServerMonitor.dummy_status = dummy_status


def reset_dsm_config(**kwargs):
    """Reset the DummyServerMonitor ConfigMsg and update with kwargs"""
    DummyServerMonitor.dummy_config = dataclasses.replace(dummy_config, **kwargs)


def reset_dsm_status(**kwargs):
    """Reset the DummyServerMonitor StatusMsg and update with kwargs"""
    DummyServerMonitor.dummy_status = dataclasses.replace(dummy_status, **kwargs)


@dataclass
class FmcMergeScenarioBase(ABC):
    """Base class for test_server_fmc_merge() scenarios"""

    testid: str

    @abstractmethod
    def update_dummy(self):
        """Update DummyServerMonitor for the scenario"""
        ...

    @property
    @abstractmethod
    def fmc_kwargs(self) -> dict[str, Any]:
        """kwargs to update FMConfig"""
        ...


@dataclass
class ScenarioException:
    """Scenario exception and match string for testing in pytest.raises()"""

    exc: Type[Exception]
    exc_match: str = ".*"


ScenarioCaseT = tuple[FmcMergeScenarioBase, ScenarioException | None]


@dataclass
class AziScenario(FmcMergeScenarioBase):
    """Scenario for merging server config and FMConfig azimuth settings"""

    server_start_azid: float
    server_end_azid: float
    server_meas_rays: int = 1703
    fmc_start_azi: float = 0
    fmc_end_azi: float | None = None
    fmc_srays: int | None = None

    @staticmethod
    def deg2azival(deg: float):
        """Convert degrees to 16-bit azimuth value"""
        return int((deg / 360) * 65536)

    @property
    def server_start_azival(self):
        return self.deg2azival(self.server_start_azid)

    @property
    def server_end_azival(self):
        return self.deg2azival(self.server_end_azid)

    @property
    def fmc_kwargs(self):
        return {
            "start_azi": self.fmc_start_azi,
            "end_azi": self.fmc_end_azi,
            "srays_per_frame": self.fmc_srays,
        }

    def update_dummy(self):
        reset_dsm_config(
            start_azival=self.server_start_azival, end_azival=self.server_end_azival
        )


@dataclass
class RangeScenario(FmcMergeScenarioBase):
    """Scenario for merging server config and FMConfig range settings"""

    server_num_samples: int
    server_start_range_metres: float
    fmc_min_range: float | None = None
    fmc_max_range: float | None = None
    fmc_num_samples: int | None = None
    server_calc_start_range_metres: float = dataclasses.field(init=False)
    server_calc_end_range_metres: float = dataclasses.field(init=False)

    def __post_init__(self):
        m_per_sample = 2.998
        self.server_calc_start_range_metres = (
            self.server_start_range_metres // m_per_sample
        ) * m_per_sample
        self.server_calc_end_range_metres = (
            self.server_calc_start_range_metres + self.server_num_samples * m_per_sample
        )

    def update_dummy(self):
        reset_dsm_config(
            num_samples=self.server_num_samples,
            start_range_metres=self.server_start_range_metres,
            calc_start_range_metres=self.server_calc_start_range_metres,
            calc_end_range_metres=self.server_calc_end_range_metres,
        )

    @property
    def fmc_kwargs(self):
        return {
            "min_range": self.fmc_min_range,
            "max_range": self.fmc_max_range,
            "num_samples": self.fmc_num_samples,
        }


@dataclass
class GeneralScenario(FmcMergeScenarioBase):
    """Scenario for merging other server config and FMConfig settings"""

    dummyconf_kwargs: dict | None = None
    fmc_kwargs_: dict | None = None

    def update_dummy(self):
        if self.dummyconf_kwargs:
            reset_dsm_config(**self.dummyconf_kwargs)

    @property
    def fmc_kwargs(self):
        """kwargs to update FMConfig"""
        return self.fmc_kwargs_ or {}


#: AziScenario test cases
azi_scenarios: list[ScenarioCaseT] = [
    # No exception tests
    (AziScenario("azi_both_360", 0, 0), None),
    (AziScenario("azi_server_offset_90_fmc_360", 90, 90), None),
    (AziScenario("azi_fmc_offset_90", 0, 0, fmc_start_azi=90), None),
    (AziScenario("azi_fmc_wrapping", 0, 0, fmc_start_azi=345, fmc_end_azi=30), None),
    (AziScenario("azi_server_wrapping", 89, 0, fmc_start_azi=90, fmc_end_azi=340), None),
    (
        AziScenario("azi_fmc_within_server", 45, 270, fmc_start_azi=45, fmc_end_azi=250),
        None,
    ),
    (
        AziScenario(
            "azi_fmc_within_server_wrapping", 270, 80, fmc_start_azi=340, fmc_end_azi=45
        ),
        None,
    ),
    (AziScenario("azi_srays_360_deg", 0, 0, fmc_srays=269), None),
    (AziScenario("azi_srays_270_deg", 0, 270, fmc_srays=202), None),
    (
        AziScenario("azi_srays_wrap_270_deg", 150, 60, fmc_start_azi=180, fmc_srays=179),
        None,
    ),
    # Exception tests
    (
        AziScenario("azi_server_start_90_fmc_360", 90, 0),
        ScenarioException(
            FMConfigError,
            ".* 360-degree coverage .*",
        ),
    ),
    (
        AziScenario("azi_server_end_90_fmc_360", 0, 90),
        ScenarioException(
            FMConfigError,
            ".* 360-degree coverage .*",
        ),
    ),
    (
        AziScenario(
            "azi_fmc_start_outside_server",
            45,
            270,
            fmc_start_azi=22,
            fmc_end_azi=250,
        ),
        ScenarioException(FMConfigError, ".*requested start azimuth.*"),
    ),
    (
        AziScenario(
            "azi_fmc_end_outside_server",
            45,
            270,
            fmc_start_azi=46,
            fmc_end_azi=40,
        ),
        ScenarioException(FMConfigError, ".*requested or calculated end azimuth.*"),
    ),
    (
        AziScenario("azi_srays_gt_360", 0, 0, fmc_srays=271),
        ScenarioException(FMConfigError, ".* more than 360 degrees.* "),
    ),
    (
        AziScenario("azi_srays_gt_270", 0, 270, fmc_srays=203),
        ScenarioException(FMConfigError, ".* calculated end azimuth.* "),
    ),
    (
        AziScenario(
            "azi_srays_wrap_gt_270_deg",
            150,
            60,
            fmc_start_azi=180,
            fmc_srays=180,
        ),
        ScenarioException(FMConfigError, ".*calculated end azimuth.*"),
    ),
    (
        AziScenario(
            "azi_cross_zero_mismatch_a", 45, 315, fmc_start_azi=310, fmc_end_azi=50
        ),
        ScenarioException(FMConfigError, ".*eclipse.*"),
    ),
    (
        AziScenario(
            "azi_cross_zero_mismatch_b", 315, 45, fmc_start_azi=40, fmc_end_azi=320
        ),
        ScenarioException(FMConfigError, ".*eclipse.*"),
    ),
    (
        AziScenario(
            "azi_srays_cross_zero_mismatch", 315, 45, fmc_start_azi=40, fmc_srays=210
        ),
        ScenarioException(FMConfigError, ".*eclipse.*"),
    ),
]


#: RangeScenario test cases
range_scenarios: list[ScenarioCaseT] = [
    # Passing cases
    (RangeScenario("rg_full_4k", 4096, 0), None),
    (RangeScenario("rg_full_4k_start_500", 4096, 500), None),
    (RangeScenario("rg_4ks_first_2ks", 4096, 0, fmc_num_samples=2048), None),
    (RangeScenario("rg_4ks_last_2ks", 4096, 6139, fmc_num_samples=2048), None),
    (
        RangeScenario(
            "rg_4ks_last_2ks_with_fmc_min",
            4096,
            0,
            fmc_min_range=6139,
            fmc_num_samples=2048,
        ),
        None,
    ),
    (RangeScenario("rg_4ks_fmc_max", 4096, 0, fmc_max_range=12279), None),
    (
        RangeScenario(
            "rg_4ks_fmc_min_max", 4096, 0, fmc_min_range=500, fmc_max_range=12279
        ),
        None,
    ),
    (
        RangeScenario("rg_4ks_serverstart_500_fmc_4ks", 4096, 500, fmc_num_samples=4096),
        None,
    ),
    # Exception Cases
    (
        RangeScenario(
            "rg_fmc_ns_and_max_rg", 4096, 0, fmc_num_samples=2048, fmc_max_range=12279
        ),
        ScenarioException(FMConfigError, ".*Only one of num_samples and max_range.*"),
    ),
    (
        RangeScenario("rg_min_rg_too_small", 4096, 1000, fmc_min_range=0),
        ScenarioException(FMConfigError, ".*Specified FrameMaker min_range.*"),
    ),
    (
        RangeScenario("rg_max_rg_too_big", 4096, 0, fmc_max_range=12280),
        ScenarioException(FMConfigError, ".*Specified FrameMaker max_range.*"),
    ),
    (
        RangeScenario("rg_4ks_fmc_5ks", 4096, 0, fmc_num_samples=5120),
        ScenarioException(FMConfigError, ".*Requested number of samples.*"),
    ),
    (
        RangeScenario(
            "rg_4ks_fmcmin_50_fmc_4ks", 4096, 0, fmc_min_range=50, fmc_num_samples=4096
        ),
        ScenarioException(FMConfigError, ".*Requested number of samples.*"),
    ),
]


#: GeneralScenario test cases
other_scenarios: list[ScenarioCaseT] = [
    (
        GeneralScenario(
            "frame_max_nrays_factor", fmc_kwargs_={"frame_max_nrays_factor": 0.5}
        ),
        ScenarioException(FMConfigError, ".*allocate less than the measured number.*"),
    ),
    (
        GeneralScenario("framebuf_size", fmc_kwargs_={"framebuf_size": 1}),
        ScenarioException(FMConfigError, ".*not be less than 2.*"),
    ),
]

tsfm_args: list[ScenarioCaseT] = azi_scenarios + range_scenarios + other_scenarios

tsfm_argids = [asn[0].testid for asn in tsfm_args]


@pytest.mark.parametrize("fmc_merge_scenario,scenario_exc", tsfm_args, ids=tsfm_argids)
def test_server_fmc_merge(
    monkeypatch,
    fmc_merge_scenario: FmcMergeScenarioBase,
    scenario_exc: ScenarioException | None,
):
    """Test how the server config and FrameMaker config are merged"""
    # Eventually break this into chunks, maybe use pytest parameterize
    monkeypatch.setattr(framemaker._fmconfig, "ServerMonitor", DummyServerMonitor)
    fmc_merge_scenario.update_dummy()
    if scenario_exc is None:
        _ = FMConfig("", **fmc_merge_scenario.fmc_kwargs)
    else:
        with pytest.raises(scenario_exc.exc, match=scenario_exc.exc_match):
            _ = FMConfig("", **fmc_merge_scenario.fmc_kwargs)


def test_server_rg(monkeypatch):
    monkeypatch.setattr(framemaker._fmconfig, "ServerMonitor", DummyServerMonitor)
    reset_dsm_config()
    server_m_per_sample = (
        dummy_config.calc_end_range_metres - dummy_config.calc_start_range_metres
    ) / dummy_config.num_samples
    fmc = FMConfig("")
    assert len(fmc.rg) == dummy_config.num_samples
    assert fmc.rg[0] == dummy_config.calc_start_range_metres
    assert fmc.rg[-1] == dummy_config.calc_end_range_metres

    fmc = FMConfig("", num_samples=200)
    assert len(fmc.rg) == 200

    fmc = FMConfig("", max_range=1000)
    assert abs(fmc.rg[-1] - 1000) < server_m_per_sample

    fmc = FMConfig("", min_range=1000)
    assert abs(fmc.rg[0] - 1000) < server_m_per_sample
