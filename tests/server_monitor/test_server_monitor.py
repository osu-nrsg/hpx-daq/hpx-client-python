import pytest

from hpx_client import ServerMonitor, ServerMonitorError


def test_invalid_endpoint():
    with pytest.raises(ServerMonitorError):
        servmon = ServerMonitor("", 1.0)
        servmon.shutdown()
