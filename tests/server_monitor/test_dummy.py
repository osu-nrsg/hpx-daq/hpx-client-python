import time

import pytest

from hpx_client import (
    ConfigMsg,
    DummyServerMonitor,
    ServerConfigChanged,
    ServerState,
    ServerStatusTimeout,
    StatusMsg,
    TpgMode,
)

dummy_status = StatusMsg(
    state=ServerState.running,
    avg_prf=1210.6830673866195,
    meas_azimuths=359,
    meas_rays=1703,
    runtime_s=3,
    message="",
)
dummy_config = ConfigMsg(
    board_idx=0,
    analog_channel="A",
    num_samples=4096,
    start_range_metres=478.0,
    range_correction_metres=-1400.0,
    voltage_offset=2.75,
    gain=-1.0,
    tpg_enabled=False,
    tpg_trigger_frequency=2000.0,
    tpg_acp_frequency=2560.0,
    tpg_arp_frequency=2048,
    enable_azimuth_interpolation=True,
    status_and_config_period_ms=500,
    spokes_per_interrupt=-1,
    interrupts_per_second=-1,
    calc_true_timestamps=True,
    summing_factor=6,
    start_azival=0,
    end_azival=0,
    send_filtered=True,
    card_name="HPx-200",
    fpga_version=17,
    calc_start_range_metres=476.7007870800003,
    calc_end_range_metres=12756.199866760002,
    meas_spokes_per_interrupt=41,
    version="0.5.0",
    tpg_mode=TpgMode.analog_ramp,
    bit_depth=12,
)

endpoint = "ipc:///run/hpx-radar-server/hpx-pub.sock"


def test_dummy_basic():
    DummyServerMonitor.dummy_status = dummy_status
    DummyServerMonitor.dummy_config = dummy_config
    with DummyServerMonitor(endpoint, 1.0) as servmon:
        servmon.wait_status_config()
        for i in range(4):
            servmon.check_and_raise()
            if i < 3:
                time.sleep(0.5)


def test_dummy_status_timeout():
    DummyServerMonitor.dummy_status = dummy_status
    DummyServerMonitor.dummy_config = dummy_config
    with DummyServerMonitor(endpoint, 1.0) as servmon:
        servmon.wait_status_config()
        servmon.check_and_raise()
        servmon.toggle_status(False)
        time.sleep(2)  # enough time to register in _monitor
        with pytest.raises(ServerStatusTimeout):
            servmon.check_and_raise()


def test_dummy_config_changed():
    DummyServerMonitor.dummy_status = dummy_status
    DummyServerMonitor.dummy_config = dummy_config
    with DummyServerMonitor(endpoint, 1.0) as servmon:
        servmon.wait_status_config()
        servmon.check_and_raise()
        servmon.update_config(gain=0)
        time.sleep(1.0)  # enough time to register in _monitor
        with pytest.raises(ServerConfigChanged):
            servmon.check_and_raise()


if __name__ == "__main__":
    test_dummy_basic()
